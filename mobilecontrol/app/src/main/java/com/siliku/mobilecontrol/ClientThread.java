package com.siliku.mobilecontrol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class ClientThread implements Runnable {

    private Socket socket;

    private static int SERVERPORT;
    private static String SERVER_IP;

    public String readline = " ";
    private BufferedReader input;
    public String led1,led2,ldr;
    public String endstate = "null";

    @Override
    public void run() {

        try {
            System.out.println(SERVER_IP);
            System.out.println(SERVERPORT);
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            socket = new Socket(serverAddr, SERVERPORT);

            this.input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

        }catch (Exception e){

        }

        while(!Thread.currentThread().isInterrupted()){
            try{
                readline = input.readLine();
                if(readline.equals("close light 1")){
                    led1 = "close";
                }
                if(readline.equals("close light 2")){
                    led2 = "close";
                }
                if(readline.equals("open light 1")){
                    led1 = "open";
                }
                if(readline.equals("open light 2")){
                    led2 = "open";
                }
                if(readline.equals("ldr on")){
                    ldr = "open";
                }
                if(readline.equals("ldr off")){
                    ldr = "close";
                }
                else{
                    System.out.println(readline);
                    endstate = readline;
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public void set_server_ip(String ipserver){
        SERVER_IP = ipserver;
    }

    public void set_server_port(int serverport){
        SERVERPORT = serverport;
    }

    public void sentdata(String data){
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
            out.println(data);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void disconnect(){
        try {
            socket.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
