package com.siliku.mobilecontrol;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class view extends Activity implements SensorEventListener {

    private Boolean checkcn1 = false;
    private Boolean checkcn2 = false;
    ClientThread client = new ClientThread();
    Thread thread = new Thread(client);
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showactivity_view();
        try {
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showactivity_view(){
        setContentView(R.layout.activity_view);

        //edit text Ip address
        final EditText editipaddress = (EditText)findViewById(R.id.ipadd);
        //edit text Port
        final EditText editport = (EditText)findViewById(R.id.port);
        //button connect
        final Button buttonconnect = (Button)findViewById(R.id.connect);

        readwritefile data = new readwritefile();
        editipaddress.setText(data.read("ipaddress"));
        editport.setText(data.read("port"));

        buttonconnect.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                try {
                    readwritefile data = new readwritefile();
                    data.write("ipaddress",editipaddress.getText().toString());
                    if(data.write("ipaddress", editipaddress.getText().toString())){
                        System.out.println("ipaddress save complete");
                    }else{
                        System.out.println("I/O ERROR");
                    }

                    data.write("port",editport.getText().toString());
                    if(data.write("port", editport.getText().toString())){
                        System.out.println("port save complete");
                    }else{
                        System.out.println("I/O ERROR");
                    }

                    int portnum = Integer.parseInt(editport.getText().toString());
                    new ClientThread().set_server_ip(editipaddress.getText().toString());
                    new ClientThread().set_server_port(portnum);
                    thread.start();
                    waiteconnect();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void waiteconnect(){
        try {
            setContentView(R.layout.p_waite);
            final Button go1 = (Button) findViewById(R.id.btgo);
            go1.setEnabled(false);
            Thread.sleep(2000);
            client.sentdata("showstate\r\n");
            while(!client.readline.equals("endshowstate"));
            go1.setEnabled(true);
            go1.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    conArduino();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void conArduino(){
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
        setContentView(R.layout.mainprogram);
        try {
            //-------------------------------------------------------
            // button cn1
            final Button cn1 = (Button) findViewById(R.id.led1);
            //--------------------------------------------------------

            //--------------------------------------------------------
            // button cn2
            final Button cn2 = (Button) findViewById(R.id.led2);
            //--------------------------------------------------------

            // button dis con
            final Button discon = (Button) findViewById(R.id.discon);
            final ToggleButton bldr = (ToggleButton)findViewById(R.id.butldr);

            discon.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    client.sentdata("exit\r\n");
                    client.disconnect();
                    System.exit(0);
                }
            });
            if(client.led1.equals("open")){
                cn1.setText("Channel1_ON");
                cn1.setBackgroundColor(Color.parseColor("#FF0000"));
                checkcn1 = true;
            }
            else{
                cn1.setText("Channel1_CLOSE");
                cn1.setBackgroundColor(Color.parseColor("#996633"));
                checkcn1 = false;
            }
            if(client.led2.equals("open")){
                cn2.setText("Channel2_ON");
                cn2.setBackgroundColor(Color.parseColor("#FF0000"));
                checkcn2 = true;
            }
            else{
                cn2.setText("Channel2_CLOSE");
                cn2.setBackgroundColor(Color.parseColor("#996633"));
                checkcn2 = false;
            }
            if(client.ldr.equals("open")){
                bldr.setChecked(true);
            }
            else{
                bldr.setChecked(false);
            }
            bldr.setOnClickListener(new Button.OnClickListener(){
                public void onClick(View v){
                    if(bldr.isChecked()){
                        client.sentdata("ldron\r\n");
                    }else{
                        client.sentdata("ldroff\r\n");
                    }
                }
            });
            cn1.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    if (checkcn1) {
                        client.sentdata("close1\r\n");
                        cn1.setText("Channel1_CLOSE");
                        cn1.setBackgroundColor(Color.parseColor("#996633"));
                        checkcn1 = false;
                    }else{
                        client.sentdata("open1\r\n");
                        cn1.setText("Channel1_ON");
                        cn1.setBackgroundColor(Color.parseColor("#FF0000"));
                        checkcn1 = true;
                    }
                }
            });
            cn2.setOnClickListener((new Button.OnClickListener(){
                public void onClick(View v){
                    if (checkcn2) {
                        client.sentdata("close2\r\n");
                        cn2.setText("Channel2_CLOSE");
                        cn2.setBackgroundColor(Color.parseColor("#996633"));
                        checkcn2 = false;
                    } else{
                        client.sentdata("open2\r\n");
                        cn2.setText("Channel2_ON");
                        cn2.setBackgroundColor(Color.parseColor("#FF0000"));
                        checkcn2 = true;
                    }
                }
            }));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final Button cn1 = (Button) findViewById(R.id.led1);
        final Button cn2 = (Button) findViewById(R.id.led2);

        float x = event.values[0];
        if(x >= 15){

            cn1.setText("Channel1_ON");
            cn1.setBackgroundColor(Color.parseColor("#FF0000"));
            checkcn1 = true;

            cn2.setText("Channel2_ON");
            cn2.setBackgroundColor(Color.parseColor("#FF0000"));
            checkcn1 = true;

            client.sentdata("open1\r\n");
            client.sentdata("open2\r\n");
        }
        if(x <= -15){

            cn2.setText("Channel2_CLOSE");
            cn2.setBackgroundColor(Color.parseColor("#996633"));
            checkcn2 = false;

            cn1.setText("Channel1_CLOSE");
            cn1.setBackgroundColor(Color.parseColor("#996633"));
            checkcn1 = false;

            client.sentdata("close1\r\n");
            client.sentdata("close2\r\n");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
