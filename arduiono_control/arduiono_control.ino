#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

String time = "";

byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

EthernetServer server(23);
boolean alreadyConnected = false; 

unsigned int localPort = 8888;
IPAddress timeServer(132, 163, 4, 101);
const int NTP_PACKET_SIZE= 48;
byte packetBuffer[ NTP_PACKET_SIZE];
EthernetUDP Udp;

int relay0 = 22;
int relay1 = 23;
int ldr = 24;
int pb0 = 26;
int pb1 = 27;
int pb0toggle = 0;
int pb1toggle = 0;
int memrelay0 = 0;
int memrelay1 = 1;
int switchldr = 2;
int ledstatcon = A2;

void setup() {
  pinMode(relay0, OUTPUT);
  pinMode(relay1, OUTPUT);
  pinMode(ldr, INPUT);
  pinMode(pb0, INPUT);
  pinMode(pb1, INPUT);
  pinMode(ledstatcon, OUTPUT);
  
  digitalWrite(relay0,LOW);
  digitalWrite(relay1,LOW);
  digitalWrite(ledstatcon,LOW);
  
  Serial.begin(9600);
   while (!Serial) {
    ;
  }
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    for(;;)
      ;
  }
  server.begin();
  Udp.begin(localPort);
  lcd.begin(16, 2);

  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
  lcd.setCursor(0, 0);
  lcd.print("                ");
  lcd.setCursor(0, 1);
  lcd.print("                ");
  checkstate();
}
int loopsyctime = 0;
void loop() {
  digitalWrite(ledstatcon,LOW);
  EthernetClient client = server.available();
  if(client) {
    String clientMsg ="";
    server.println("Hello Client");
    while(client.connected()) {
      digitalWrite(ledstatcon,HIGH);
      printlcdstaterelay();
      if(client.available()) {
        char c = client.read();
        clientMsg+=c;
        if(c == '\n'){
          Serial.print(clientMsg);
          if(clientMsg == "showtime\r\n"){
            server.println(time);
            Serial.println(time);
            server.println("endstate");
            Serial.println("endstate");
          }
          else if(clientMsg == "showstate\r\n"){
            checkstate();
            Serial.println("endshowstate");
            server.println("endshowstate");
          }
          else if(clientMsg == "open1\r\n"){
            digitalWrite(relay0,HIGH);
            EEPROM.write(memrelay0,1);
            Serial.println("open light 1");
            server.println("open light 1");
          }
          else if(clientMsg == "open2\r\n"){
            digitalWrite(relay1,HIGH);
            EEPROM.write(memrelay1,1);
            Serial.println("open light 2");
            server.println("open light 2");
          }
          else if(clientMsg == "close1\r\n"){
            digitalWrite(relay0,LOW);
            EEPROM.write(memrelay0,0);
            Serial.println("close light 1");
            server.println("close light 1");
          }
          else if(clientMsg == "close2\r\n"){
            digitalWrite(relay1,LOW);
            EEPROM.write(memrelay1,0);
            Serial.println("close light 2");
            server.println("close light 2");
          }
          else if(clientMsg == "ldron\r\n"){
            EEPROM.write(switchldr,1);
            Serial.println("ldr on");
            server.println("ldr on");
          }
          else if(clientMsg == "ldroff\r\n"){
            EEPROM.write(switchldr,0);
            Serial.println("ldr off");
            server.println("ldr off");
          }
          else if(clientMsg == "exit\r\n"){
            server.println("Good bye");
            client.stop();
          }
          clientMsg="";
        }
      }
    } 
  }
  loopsyctime++;
  if(loopsyctime == 200){
    printlcdstaterelay();
    syctime(); 
    manaulswitch();
    loopsyctime = 0;
    Serial.println(time);
  }
  manaulswitch();
  lcd.setCursor(0, 0);
  lcd.print(Ethernet.localIP());
  lcd.print("    ");
  lcd.setCursor(0, 1);
  lcd.print(time);
  lcd.print(" ldr ");
  if(EEPROM.read(switchldr) == 1){
    lcd.print("on");
  }
  else{
    lcd.print("off");
  }
}

void manaulswitch(){
  if(digitalRead(pb0) == LOW && pb0toggle == 0){
    digitalWrite(relay0,HIGH);
    EEPROM.write(memrelay0,1);
    Serial.println("open light 1");
    server.println("open light 1");
    pb0toggle = 1;
    delay(1000);
  }
  else if(digitalRead(pb0) == LOW && pb0toggle == 1){
    digitalWrite(relay0,LOW);
    EEPROM.write(memrelay0,0);
    Serial.println("close light 1");
    server.println("close light 1");
    pb0toggle = 0;
    delay(1000);
  }
  if(digitalRead(pb1) == LOW && pb1toggle == 0){
    digitalWrite(relay1,HIGH);
    EEPROM.write(memrelay1,1);
    Serial.println("open light 2");
    server.println("open light 2");
    pb1toggle = 1;
    delay(1000);
  }
  else if(digitalRead(pb1) == LOW && pb1toggle == 1){
    digitalWrite(relay1,LOW);
    EEPROM.write(memrelay1,0);
    Serial.println("close light 2");
    server.println("close light 2");
    pb1toggle = 0;
    delay(1000);
  }
  if(digitalRead(ldr) == HIGH && EEPROM.read(switchldr) == 1){
    digitalWrite(relay0,HIGH);
    EEPROM.write(memrelay0,1);
    digitalWrite(relay1,HIGH);
    EEPROM.write(memrelay1,1);
  }
  else if(digitalRead(ldr) == LOW && EEPROM.read(switchldr) == 1){
    digitalWrite(relay0,LOW);
    EEPROM.write(memrelay0,0);
    digitalWrite(relay1,LOW);
    EEPROM.write(memrelay1,0);
  }
}

void checkstate(){
  if(EEPROM.read(memrelay0) == 0){
    digitalWrite(relay0,LOW);
    Serial.println("close light 1");
    server.println("close light 1");
  }
  else if(EEPROM.read(memrelay0) == 1){
    digitalWrite(relay0,HIGH);
    Serial.println("open light 1");
    server.println("open light 1");
  }
  if(EEPROM.read(memrelay1) == 0){
    digitalWrite(relay1,LOW);
    Serial.println("close light 2");
    server.println("close light 2");
  }
  else if(EEPROM.read(memrelay1) == 1){
    digitalWrite(relay1,HIGH);
    Serial.println("open light 2");
    server.println("open light 2");
  }
  if(EEPROM.read(switchldr) == 1){
    Serial.println("ldr on");
    server.println("ldr on");
  }
  else if(EEPROM.read(switchldr) == 0){
    Serial.println("ldr off");
    server.println("ldr off");
  }
}

void printlcdstaterelay(){
    if(EEPROM.read(memrelay0) == 0){
      lcd.setCursor(0, 0);
      lcd.print("relay 0 is off ");
    }
    else if(EEPROM.read(memrelay0) == 1){
      lcd.setCursor(0, 0);
      lcd.print("relay 0 is on  ");
    }
    if(EEPROM.read(memrelay1) == 0){
      lcd.setCursor(0, 1);
      lcd.print("relay 1 is off ");
    }
    else if(EEPROM.read(memrelay1) == 1){
      lcd.setCursor(0, 1);
      lcd.print("relay 1 is on  ");
    }
}

void syctime(){
  sendNTPpacket(timeServer);
  time = "";
  delay(1000);  
  if ( Udp.parsePacket() ) {  
    Udp.read(packetBuffer,NTP_PACKET_SIZE); 
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);  
    unsigned long secsSince1900 = highWord << 16 | lowWord;  
    const unsigned long seventyYears = 2208988800UL;     
    unsigned long epoch = secsSince1900 - seventyYears + (7*3600);  
    time = time + String((epoch  % 86400L) / 3600) + ":";
    if ( ((epoch % 3600) / 60) < 10 ) {
      time = time + '0';
    }
    time = time + String((epoch  % 3600) / 60) + ":";
    if ( (epoch % 60) < 10 ) {
      time = time + '0';
    }
    time = time + String(epoch %60);
  }
  delay(1000); 
}

unsigned long sendNTPpacket(IPAddress& address)
{
  memset(packetBuffer, 0, NTP_PACKET_SIZE); 
  packetBuffer[0] = 0b11100011;   
  packetBuffer[1] = 0;     
  packetBuffer[2] = 6;     
  packetBuffer[3] = 0xEC; 
  packetBuffer[12]  = 49; 
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;		   
  Udp.beginPacket(address, 123);
  Udp.write(packetBuffer,NTP_PACKET_SIZE);
  Udp.endPacket(); 
}



